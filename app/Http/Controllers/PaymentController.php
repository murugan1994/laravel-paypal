<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaymentController extends Controller
{
    public function execute(){
		// After Step 1
		$apiContext = new \PayPal\Rest\ApiContext(
				new \PayPal\Auth\OAuthTokenCredential(
					'AfnBXULVJM0pyvzJNj24fyIg-gy4JSwbRLL0eZ1_RVzgteGVYj7NHp4a_PaSle4jirCYe_Bvt4g-GDAi',     // ClientID
					'EIvdeQuRkRUrF3aD1ghCSnHXvf2Ggex5BQGcg_Z6K9MqRKe7fh-PFrI8KyXBJulTf33554Fp5ET-78XB'      // ClientSecret
				)
		);
		
		$paymentId = request('paymentId');
		$payment = Payment::get($paymentId, $apiContext);

		$execution = new PaymentExecution();
		$execution->setPayerId(request('PayerID'));
		
		$transaction = new Transaction();
		$amount = new Amount();
		$details = new Details();
		
		$details->setShipping(1)
			->setTax(1)
			->setSubtotal(2);
		
		$amount->setCurrency('USD');
		$amount->setTotal(4);
		$amount->setDetails($details);
		$transaction->setAmount($amount);
		
		$execution->addTransaction($transaction);
		
		try {

			$result = $payment->execute($execution, $apiContext);

			//print_r(array("Executed Payment", "Payment", $payment->getId(), $execution, $result));

			try {
				$payment = Payment::get($paymentId, $apiContext);
			} catch (Exception $ex) {

				print_r(array("Get Payment", "Payment", null, null, $ex));
				exit(1);
			}
		} catch (Exception $ex) {

			print_r(array("Executed Payment", "Payment", null, null, $ex));
			exit(1);
		}

		//print_r(array("Get Payment", "Payment", $payment->getId(), null, $payment));

		return $payment;

	}
}
