<div id="paypal-button"></div>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    // Configure environment
    env: 'sandbox',
    client: {
      sandbox: 'AfnBXULVJM0pyvzJNj24fyIg-gy4JSwbRLL0eZ1_RVzgteGVYj7NHp4a_PaSle4jirCYe_Bvt4g-GDAi',
      production: 'demo_production_client_id'
    },
    // Customize button (optional)
    locale: 'en_US',
    style: {
      size: 'small',
      color: 'gold',
      shape: 'pill',
    },

    // Enable Pay Now checkout flow (optional)
    commit: true,

    // Set up a payment
    payment: function(data, actions) {
      return actions.payment.create({
		redirect_urls:{
			return_url: 'http://localhost/repository/task/task1/app/public/execute-payment/',
		},
        transactions: [{
          amount: {
            total: '1',
            currency: 'USD'
          }
        }]
      });
    },
    // Execute the payment
    onAuthorize: function(data, actions) {
		return actions.redirect();
      /* return actions.payment.execute().then(function() {
        // Show a confirmation message to the buyer
        window.alert('Thank you for your purchase!');
      }); */
    }
  }, '#paypal-button');

</script>